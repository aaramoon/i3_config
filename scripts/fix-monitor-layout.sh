#!/bin/bash


DELL_E2414H=$(grep GPU /var/log/Xorg.0.log | grep -i "DELL" | grep "E2414H" | wc -l)
DELL_P2417H=$(grep GPU /var/log/Xorg.0.log | grep -i "DELL" | grep "P2417H" | wc -l)

NMON=$(xrandr --listmonitors | head -n1 | grep -oP '(?<=Monitors: )[0-9]+')

if [ $DELL_E2414H -gt 0 -a $DELL_P2417H -gt 0 ]
then
	ENV="home"
else
	ENV="work"
fi

if [ $ENV == "home" ]
then
	xrandr --output DP-0 --primary
    	xrandr --output DP-1 --right-of DP-0
    	xrandr --output eDP-1-1 --right-of DP-1
else
	xrandr --output DP-0 --primary
	xrandr --output DP-2 --right-of DP-0
	xrandr --output eDP-1-1 --right-of DP-2
fi

